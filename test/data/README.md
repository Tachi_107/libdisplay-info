# Test data

The following blobs originate from the [linuxhw/EDID] repository and are
licensed under the [Creative Commons Attribution 4.0 International][CC-BY-4.0]
license:

- panasonic-mei96a2-dp
- samsung-s27a950d-dp
- sun-gh19ps-dvi

The other blobs are under the same license as the rest of the libdisplay-info
project.

[linuxhw/EDID]: https://github.com/linuxhw/EDID
[CC-BY-4.0]: LICENSE.CC-BY-4.0
